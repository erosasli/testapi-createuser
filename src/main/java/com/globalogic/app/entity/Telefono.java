package com.globalogic.app.entity;

import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import org.hibernate.annotations.GenericGenerator;

/**
 * 
 * Clase para modelo de entidad telefono
 * 
 * 
 * @author erosasli
 * @See org.hibernate.id.UUIDGenerator
 *
 */
@Entity
public class Telefono {

	@Id
	@GeneratedValue(generator = "UUID")
	@GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
	@Column(name = "id", updatable = false, nullable = false)
	private UUID id;
	
	

	@Column(name = "number")
	private Integer number;

	@Column(name = "citycode")
	private Integer cityCode;

	@Column(name = "contrycode")
	private Integer contryCode;

	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}

	public Integer getNumber() {
		return number;
	}

	public void setNumber(Integer number) {
		this.number = number;
	}

	public Integer getCityCode() {
		return cityCode;
	}

	public void setCityCode(Integer cityCode) {
		this.cityCode = cityCode;
	}

	public Integer getContryCode() {
		return contryCode;
	}

	public void setContryCode(Integer contryCode) {
		this.contryCode = contryCode;
	}

}